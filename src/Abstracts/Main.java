package Abstracts;

import java.util.Random;

public class Main {
        public static void main(String[] args) {
                Figure[] figures = new Figure[2];
                figures[0] = new Square();
                {
                        System.out.println("Начальный периметр: " + figures[0].getPerimeter()); // Текущий периметр
                }
                figures[1] = new Circle();
                {
                        System.out.println("Начальный периметр: " + figures[1].getPerimeter());
                }
                Random random = new Random();
                int r = random.nextInt(100);
                {
                        for (Figure figure : figures) {
                                if (figure instanceof Moveable) {
                                        ((Moveable) figure).move(r, r);
                                }
                        }
                }

                System.out.println("Итоговый периметр квадрата: " + figures[0].getPerimeter());
                System.out.println("Итоговый периметр круга: " + figures[1].getPerimeter());
        }
}