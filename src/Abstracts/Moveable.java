package Abstracts;

public interface Moveable {
    void move (int x, int y);
}
